import UIKit
import Stevia

class TableEmptyView: UIView {
    private lazy var messageLabel = UILabel().style(messageStyle)
    private func messageStyle(l: UILabel) {
        l.textAlignment = .center
        l.font = Font.title.font
        l.textColor = R.color.black()
        l.text = message
        l.numberOfLines = 0
    }

    var message: String? {
        didSet {
            DispatchQueue.main.async {
                self.messageLabel.text = self.message
            }
        }
    }

    init(message: String) {
        self.message = message
        super.init(frame: .zero)
        setupSubview()
    }
    required init?(coder aDecoder: NSCoder) { fatalError() }

    private func setupSubview() {
        sv(messageLabel)
        messageLabel.centerVertically()
        layout(
            |-20-messageLabel-20-|
        )
    }
}
