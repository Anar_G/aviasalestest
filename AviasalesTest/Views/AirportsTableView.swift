import UIKit
import Stevia

protocol AirportsTableViewDelegate: AnyObject {
    func didSelectItem(_ location: Location, iata: String)
}

class AirportsTableView: UIView, UITableViewDelegate {
    private(set) lazy var table = UITableView(frame: .zero, style: .plain)
    private lazy var emptyView = TableEmptyView(message: "Грузим данные 📡 ...")
    private lazy var provider = DataProvider<[Airport], Airport>.singleSection
    private lazy var presenter = TableDataPresenter<Airport>(
        registerCell: setupTable,
        cellForRow: cellForRow
    )

    private lazy var dataSource = TableViewAdapter(
        dataProvider: provider,
        dataPresenter: presenter
    )

    var data: [Airport]? {
        didSet {
            guard let data = data else { return }
            dataSource.data = data
            chnageEmptyState(data)
            DispatchQueue.main.async {
                self.table.reloadData()
            }
        }
    }

    var errorDescription: String? {
        didSet { emptyView.message = errorDescription }
    }

    private weak var delegate: AirportsTableViewDelegate?
    private let topInset: CGFloat
    init(topInset: CGFloat, delegate: AirportsTableViewDelegate?) {
        self.topInset = topInset
        self.delegate = delegate
        super.init(frame: .zero)
        dataSource.register(table)
        table.dataSource = dataSource
        setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) { fatalError() }

    private func setupSubviews() {
        sv(table)
        table.fillContainer()
    }

    private func setupTable(_ table: UITableView) {
        table.backgroundColor = R.color.white()
        table.contentInset = .init(
            top: topInset,
            left: 0,
            bottom: 0,
            right: 0
        )
        table.delegate = self
        table.backgroundView = emptyView
        table.isUserInteractionEnabled = true
        table.separatorInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        table.separatorColor = R.color.blue()
        table.contentInsetAdjustmentBehavior = .never
        table.delaysContentTouches = false
        table.rowHeight = UITableView.automaticDimension
        table.showsHorizontalScrollIndicator = false
        table.showsVerticalScrollIndicator = false
        table.tableFooterView = UIView()
        table.register(cell: AirportCell.self)
    }

    private func chnageEmptyState(_ data: [Airport]) {
        guard data.isEmpty else { emptyView.message = nil; return }
        emptyView.message = "Ничего не найдено 😔"
    }

    private func cellForRow(table: UITableView, item: Airport, indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusable(cell: AirportCell.self, indexPath: indexPath)
        cell.data = item
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = dataSource.item(atIndexPath: indexPath) else { return }
        delegate?.didSelectItem(item.location, iata: item.iata)
    }
}
