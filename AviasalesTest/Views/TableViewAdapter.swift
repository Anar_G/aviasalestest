import UIKit

struct DataProvider<Model, Item> {
    var numberOfSections: (Model) -> Int
    var numberOfRowsInSection: (Model, Int) -> Int
    var itemForIndexPath: (Model, IndexPath) -> Item
    var indexPathForItem: (Model, Item) -> IndexPath?
}

extension DataProvider where Model == [Item], Item: Equatable {
    static var singleSection: DataProvider {
        return DataProvider(
            numberOfSections: { _ in return 1 },
            numberOfRowsInSection: { data, _ in data.count },
            itemForIndexPath: { data, indexPath in data[indexPath.row] },
            indexPathForItem: { data, item in
                guard let index = data.firstIndex(of: item) else { return nil }
                return IndexPath(row: index, section: 0)
            }
        )
    }
}

struct TableDataPresenter<Item: Equatable> {
    let registerCell: (UITableView) -> Void
    let cellForRow: (UITableView, Item, IndexPath) -> UITableViewCell
}

class TableViewAdapter<Model, Item: Equatable>: NSObject, UITableViewDataSource {
    var data: Model?

    let dataProvider: DataProvider<Model, Item>
    let dataPresenter: TableDataPresenter<Item>

    init(dataProvider: DataProvider<Model, Item>, dataPresenter: TableDataPresenter<Item>) {
        self.dataProvider = dataProvider
        self.dataPresenter = dataPresenter
    }

    func register(_ tableView: UITableView) {
        dataPresenter.registerCell(tableView)
    }

    func item(atIndexPath indexPath: IndexPath) -> Item? {
        guard let data = data else { return nil }
        return dataProvider.itemForIndexPath(data, indexPath)
    }

    func indexPath(forItem item: Item) -> IndexPath? {
        guard let data = data else { return nil }
        return dataProvider.indexPathForItem(data, item)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let data = data else { return 0 }
        return dataProvider.numberOfSections(data)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = data else { return 0 }
        return dataProvider.numberOfRowsInSection(data, section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = data else { return UITableViewCell() }
        let item = dataProvider.itemForIndexPath(data, indexPath)
        let cell = dataPresenter.cellForRow(tableView, item, indexPath)
        return cell
    }
}
