import UIKit
import Stevia

class AirportCell: UITableViewCell {
    var data: Airport? {
        didSet { setupData() }
    }

    private lazy var airportName = UILabel().style(nameStyle)
    private lazy var locationName = UILabel().style(loacationStyle)
    private lazy var iata = UILabel().style(iataStyle)

    private func iataStyle(l: UILabel) {
        l.font = Font.iata.font
        l.textAlignment = .center
        l.textColor = R.color.black()
    }
    private func nameStyle(l: UILabel) {
        l.font = Font.name.font
        l.textAlignment = .left
        l.textColor = R.color.black()
    }
    private func loacationStyle(l: UILabel) {
        l.font = Font.location.font
        l.textAlignment = .left
        l.textColor = R.color.black()
        l.numberOfLines = 0
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: AirportCell.classId())
        selectionStyle = .gray
        contentView.backgroundColor = R.color.white()
        backgroundColor = R.color.white()
        setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) { fatalError() }

    private let container = UIView()
    private func setupSubviews() {
        sv(container, iata)
        container.sv(airportName, locationName)
        container.layout(
            20,
            |airportName|,
            8,
            |locationName|,
            20
        )
        layout(
            0,
            container,
            0
        )
        container.Left == Left + 20
        iata.centerVertically()
        iata.Right == Right - 20
        container.Right >= iata.Left - 20
        iata.setContentHuggingPriority(.required, for: .horizontal)
    }

    private func setupData() {
        guard let data = data else { return }
        iata.text = data.iata
        airportName.text = data.airportName ?? "Любой аэропорт"
        locationName.text = data.name
    }
}
