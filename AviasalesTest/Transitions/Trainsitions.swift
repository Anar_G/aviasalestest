import UIKit

class SearchViewMoveInAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toView = transitionContext.viewController(forKey: .to) as? SearchAirportsViewController else { return }
        containerView.addSubview(toView.view)
        toView.moveIn(with: 0.3) {
            transitionContext.completeTransition(true)
        }
    }
}

class SearchViewMoveOutAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to)
        else { return }
        let containerView = transitionContext.containerView
        containerView.insertSubview(toVC.view, belowSubview: fromVC.view)

        let screenBounds = UIScreen.main.bounds
        let bottomLeftCorner = CGPoint(x: 0, y: screenBounds.height)
        let finalFrame = CGRect(origin: bottomLeftCorner, size: screenBounds.size)

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                fromVC.view.frame = finalFrame
        }) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            guard !transitionContext.transitionWasCancelled else { return }
            UIApplication.shared.keyWindow?.addSubview(toVC.view)
        }
    }

}

extension SearchAirportsViewController {
    fileprivate func moveIn(with duration: TimeInterval, completion: @escaping () -> Void) {
        view.transform = CGAffineTransform(scaleX: 1.30, y: 1.30)
        view.alpha = 0

        UIView.animate(withDuration: duration, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.alpha = 1
        }, completion: { _ in
            completion()
        })
    }
}
