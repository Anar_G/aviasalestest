import UIKit

extension UITableView {
    func register<Cell>(cell: Cell.Type) where Cell: UITableViewCell {
        register(cell, forCellReuseIdentifier: cell.classId())
    }

    func register<Cell>(cells: [Cell.Type]) where Cell: UITableViewCell {
        cells.forEach { register(cell: $0) }
    }

    func register<HeaderFooter>(headerFooter: HeaderFooter.Type) where HeaderFooter: UITableViewHeaderFooterView {
        register(headerFooter, forHeaderFooterViewReuseIdentifier: headerFooter.classId())
    }

    func register<HeaderFooter>(headerFooters: [HeaderFooter.Type]) where HeaderFooter: UITableViewHeaderFooterView {
        headerFooters.forEach { register(headerFooter: $0) }
    }

    func dequeueReusable<Cell>(cell: Cell.Type) -> Cell? where Cell: UITableViewCell {
        return dequeueReusableCell(withIdentifier: cell.classId()) as? Cell
    }

    func dequeueReusable<Cell>(cell: Cell.Type, indexPath: IndexPath) -> Cell where Cell: UITableViewCell {
        return dequeueReusableCell(withIdentifier: cell.classId(), for: indexPath) as! Cell
    }

    func dequeueReusable<HeaderFooter>(headerFooter: HeaderFooter.Type) -> HeaderFooter where HeaderFooter: UITableViewHeaderFooterView {
        return dequeueReusableHeaderFooterView(withIdentifier: headerFooter.classId()) as! HeaderFooter
    }
}

extension NSObject {
    static func classId() -> String {
        return String(describing: self)
    }
}
