import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(hideKeyboard)
        )
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

extension UIViewController {
    static func customNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.navigationBar.barStyle = .black
        navigationController.navigationBar.isTranslucent = true
        navigationController.navigationBar.tintColor = R.color.white()
        navigationController.navigationBar.barTintColor = R.color.orange()
        let attributedText: [NSAttributedString.Key: Any] = [
            .foregroundColor : R.color.white()!,
            .font : Font.title.font
        ]
        navigationController.navigationBar.titleTextAttributes = attributedText
        return navigationController
    }
}
