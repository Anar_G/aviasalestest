import UIKit

enum Font {
    case title
    case iata
    case name
    case location
}

extension Font {
    var font: UIFont {
        switch self {
        case .title: return R.font.capturaRegularDemo(size: 25)!
        case .iata: return R.font.capturaBoldDemo(size: 18)!
        case .name: return R.font.capturaRegularDemo(size: 18)!
        case .location: return R.font.capturaLightDemo(size: 15)!
        }
    }
}
