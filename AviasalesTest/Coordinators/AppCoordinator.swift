import UIKit

class AppCoordinator: RootCntrollerCoordinator {
    var childViewCoordinators: [Coordinator]? = []
    var rootViewController: UIViewController {
        return navigationController
    }

    var window: UIWindow?

    private lazy var navigationController: UINavigationController = {
        return .customNavigationController()
    }()

    private let networking = BaseNetworkingService()
    private var mainView: MainViewController?
    private var searchView: SearchAirportsViewController?
    private var mapView: MapViewController?

    init(window: UIWindow) {
        self.window = window
        self.window?.rootViewController = self.rootViewController
        self.window?.makeKeyAndVisible()
    }

    func start() {
        mainView = MainViewController(delegate: self)
        mainView?.title = "Search"
        navigationController.viewControllers = [mainView!]
    }
}

extension AppCoordinator: MainViewControllerDelegate {
    func searchAirports(in city: String) {
        searchView = SearchAirportsViewController(
            searchedPoint: city,
            networkingService: networking,
            delegate: self
        )
        searchView?.modalTransitionStyle = .coverVertical
        searchView?.modalPresentationStyle = .overCurrentContext
        mainView?.present(searchView!, animated: true, completion: nil)
    }
}

extension AppCoordinator: SearchAirportsViewControllerDelegate {
    func didSelectItem(_ location: Location, iata: String) {
        mapView = MapViewController(location: location, iata: iata)
        mapView?.title = "Route"
        searchView?.dismiss(animated: true, completion: {
            self.navigationController.pushViewController(self.mapView!, animated: true)
        })
    }

    func dismissSearch() {
        searchView?.dismiss(animated: true, completion: nil)
    }
}
