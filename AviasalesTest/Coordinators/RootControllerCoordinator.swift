import Foundation
import UIKit

protocol RootViewControllerCoordinator: class {
    var rootViewController: UIViewController { get }
}

typealias RootCntrollerCoordinator = Coordinator & RootViewControllerCoordinator
