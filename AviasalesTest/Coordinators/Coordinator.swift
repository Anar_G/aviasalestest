import Foundation

protocol Coordinator: class {
    var childViewCoordinators: [Coordinator]? { get set }
}

extension Coordinator {
    func addChildCoordinator(_ childCoordinator: Coordinator) {
        self.childViewCoordinators?.append(childCoordinator)
    }

    func removeChildCoordinator(_ childCoordinator: Coordinator) {
        self.childViewCoordinators = childViewCoordinators?.filter { $0 !== childCoordinator }
    }
}

