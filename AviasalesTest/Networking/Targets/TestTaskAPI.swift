import Foundation

enum TestTaskAPI {
    case search(string: String)
}

extension TestTaskAPI: EndpointType {
    var baseURL: URL {
        guard let url = URL(string: "http://places.aviasales.ru") else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }

    var path: String {
        switch self {
        case .search: return "/places"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .search: return .get
        }
    }

    var task: HTTPTask {
        switch self {
        case let .search(city):
            return .requestWithParameters(
                bodyParams: nil,
                urlParams: ["term": city, "locale": "ru"],
                encoding: .urlEncoding
            )
        }
    }

    var headers: HTTPHeaders? {
        return nil
    }
}
