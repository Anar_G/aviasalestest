import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case put = "PUT"
}

typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case plainRequest
    case requestWithParameters(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        encoding: ParametersEncoding
    )
    case requestParametersWithHeader(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        additionalHeader: HTTPHeaders?,
        encoding: ParametersEncoding
    )
}

protocol EndpointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

