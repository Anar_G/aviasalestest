protocol AviasalesNetworkService {
    func search(_ city: String, completion: @escaping DecodingCallback<[Airport]>)
}

class BaseNetworkingService: NetworkingService<TestTaskAPI>, AviasalesNetworkService {
    func search(_ city: String, completion: @escaping DecodingCallback<[Airport]>) {
        requestWithDecoder(.search(string: city), completion: completion)
    }
}
