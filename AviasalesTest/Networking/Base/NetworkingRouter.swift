import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case put = "PUT"
}

typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case plainRequest
    case requestWithParameters(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        encoding: ParametersEncoding
    )
    case requestParametersWithHeader(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        additionalHeader: HTTPHeaders?,
        encoding: ParametersEncoding
    )
}

protocol EndpointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

typealias RouterCallback = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

protocol Router: AnyObject {
    associatedtype Target: EndpointType
    func cancel()
    func request(_ route: Target, callback: @escaping RouterCallback)
}

class NetworkingRouter<Target>: Router where Target: EndpointType {
    private var task: URLSessionTask?

    func cancel() {
        task?.cancel()
    }

    func request(_ route: Target, callback: @escaping RouterCallback) {
        let session = URLSession.shared
        do {
            let request = try describeRequest(for: route)
            Logger.log(request)
            task = session.dataTask(with: request, completionHandler: callback)
        } catch {
            callback(nil, nil, error)
        }
        self.task?.resume()
    }

    private func describeRequest(for rout: Target) throws -> URLRequest {
        var request = URLRequest(
            url: rout.baseURL.appendingPathComponent(rout.path),
            cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 60
        )
        request.httpMethod = rout.httpMethod.rawValue
        do {
            switch rout.task {
            case .plainRequest:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case let .requestParametersWithHeader(bodyParams, urlParams, additionalHeader, encoding):
                addAdditionalHeaders(additionalHeader, request: &request)
                try configureRequestParams(
                    request: &request,
                    bodyParams: bodyParams,
                    urlParams: urlParams,
                    encoding: encoding
                )
            case let .requestWithParameters(bodyParams, urlParams, encoding):
                try configureRequestParams(
                    request: &request,
                    bodyParams: bodyParams,
                    urlParams: urlParams,
                    encoding: encoding
                )
            }
            return request
        } catch {
            throw error
        }
    }

    private func configureRequestParams(request: inout URLRequest, bodyParams: Parameters?,
                                        urlParams: Parameters?, encoding: ParametersEncoding) throws {
        do {
            try encoding.encode(request: &request, bodyParams: bodyParams, urlParams: urlParams)
        } catch {
            throw error
        }
    }

    private func addAdditionalHeaders(_ headers: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = headers else { return }
        headers.forEach { request.setValue($0.value, forHTTPHeaderField: $0.key) }
    }
}
