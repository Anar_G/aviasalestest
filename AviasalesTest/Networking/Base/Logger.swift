import Foundation

enum Logger {
    static func log(_ request: URLRequest) {
        print("\n___________OUTGOING__________\n")
        defer { print("\n______________END_____________\n") }

        let absoluteString = request.url?.absoluteString ?? "__missing_url__"
        let urlComponents = URLComponents(string: absoluteString)
        let method = request.httpMethod != nil ? "\(request.httpMethod ?? "")" : "No HTTPMethod Detected"
        let path = urlComponents?.path ?? "No `path` Detected"
        let query = urlComponents?.query ?? "No `query` Detected"
        let host = urlComponents?.host ?? "No `host` Detected"

        var loggedOutputs = """
        \(absoluteString) \n
        \(method): \(path)?\(query) HTTP/1.1/ \n
        HOST: \(host)\n
        """
        request.allHTTPHeaderFields?.forEach { loggedOutputs += "\($0.key): \($0.value) \n" }
        if let body = request.httpBody {
            loggedOutputs += "\n \(String(data: body, encoding: .utf8) ?? "__NoHTTPBody__")"
        }
        print(loggedOutputs)
    }
}

