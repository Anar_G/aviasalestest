import Foundation

enum NetworkErrorDescridtion: String, Error {
    case serverError = "Server Error."
    case badRequest = "Bad request."
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
    case noInternetConnection = "Please check your internet connection."
    case unableToCast = "can't cast response to HTTPURLResponse"
    
    case unknown
    case cancelled
    case badURL
    case timedOut
    case unsupportedURL
    case cannotFindHost
    case cannotConnectToHost
    case connectionLost
    case lookupFailed
    case HTTPTooManyRedirects
    case resourceUnavailable
    case notConnectedToInternet
    case redirectToNonExistentLocation
    case badServerResponse
    case userCancelledAuthentication
    case userAuthenticationRequired
    case zeroByteResource
    case cannotDecodeRawData
    case cannotDecodeContentData
    case cannotParseResponse
    case fileDoesNotExist
    case fileIsDirectory
    case noPermissionsToReadFile
    
    case undefined
}

extension NetworkErrorDescridtion {
    var description: String {
        return self.rawValue
    }
}

enum Result<D, E> where D: Codable, E: Error {
    case success(D)
    case failure(E)
}

typealias DecodingCallback<T: Codable> = (Result<T, NetworkErrorDescridtion>) -> Void

class NetworkingService<T: EndpointType> {
    private let router = NetworkingRouter<T>()

    func requestWithDecoder<DecodingType: Codable>(
        _ target: T,
        completion: @escaping DecodingCallback<DecodingType>
    ) {
        router.request(target) { [weak self] (data, response, error) in
            guard let self = self else { return }
            if let error = error as? URLError {
                self.handleErrors(statusCode: error.errorCode, completion: completion)
                return
            }
            guard let response = response as? HTTPURLResponse else { completion(.failure(.unableToCast)); return }
            self.handleNetworkResponse(response: response, data: data, completion: completion)
        }
    }

    private func handleNetworkResponse<DecodingType: Codable>(
        response: HTTPURLResponse, data: Data?,
        completion: @escaping DecodingCallback<DecodingType>
    ) {
        switch response.statusCode {
        case 200...299:
            guard let responseData = data else { completion(.failure(.noData)); return }
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let decoded = try decoder.decode(DecodingType.self, from: responseData)
                dump(decoded)
                completion(.success(decoded))
            } catch {
                completion(.failure(.unableToDecode))
            }
        case 401...499: completion(.failure(.badRequest))
        case 500...599: completion(.failure(.serverError))
        case 600: completion(.failure(.outdated))
        default: completion(.failure(.failed))
        }
    }
    
    private func handleErrors<DecodingType: Codable>(statusCode: Int, completion: @escaping DecodingCallback<DecodingType>) {
        switch statusCode {
        case -1: completion(.failure(.unknown))
        case -999: completion(.failure(.cancelled))
        case -1000: completion(.failure(.badURL))
        case -1001: completion(.failure(.timedOut))
        case -1002: completion(.failure(.unsupportedURL))
        case -1003: completion(.failure(.cannotFindHost))
        case -1004: completion(.failure(.cannotConnectToHost))
        case -1005: completion(.failure(.connectionLost))
        case -1006: completion(.failure(.lookupFailed))
        case -1007: completion(.failure(.HTTPTooManyRedirects))
        case -1008: completion(.failure(.resourceUnavailable))
        case -1009: completion(.failure(.notConnectedToInternet))
        case -1010: completion(.failure(.redirectToNonExistentLocation))
        case -1011: completion(.failure(.badServerResponse))
        case -1012: completion(.failure(.userCancelledAuthentication))
        case -1013: completion(.failure(.userAuthenticationRequired))
        case -1014: completion(.failure(.zeroByteResource))
        case -1015: completion(.failure(.cannotDecodeRawData))
        case -1016: completion(.failure(.cannotDecodeContentData))
        case -1017: completion(.failure(.cannotParseResponse))
        case -1100: completion(.failure(.fileDoesNotExist))
        case -1101: completion(.failure(.fileIsDirectory))
        case -1102: completion(.failure(.noPermissionsToReadFile))
        default: completion(.failure(.undefined))
        }
    }
}
