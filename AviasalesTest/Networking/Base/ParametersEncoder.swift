import Foundation

typealias Parameters = [String: Any]

protocol ParametersEncoder {
    func encode(request: inout URLRequest, params: Parameters) throws
}

enum NetworkError: Error {
    case nilParametres
    case encodingError
    case missingURL
}

struct JSONParametersEncoder: ParametersEncoder {
    public func encode(request: inout URLRequest, params: Parameters) throws {
        do {
            let jsonAsData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.httpBody = jsonAsData
            if request.value(forHTTPHeaderField: "Content-Type") == nil {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        } catch {
            throw NetworkError.encodingError
        }
    }
}

struct URLParametersEncoder: ParametersEncoder {
    public func encode(request: inout URLRequest, params: Parameters) throws {
        guard let url = request.url else { throw NetworkError.missingURL }
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !params.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            params.forEach {
                if $0.key == "term" {
                    let queryItem = URLQueryItem(
                        name: $0.key,
                        value: "\($0.value)".lowercased().removingPercentEncoding
                    )
                    urlComponents.queryItems?.append(queryItem)
                } else {
                    let queryItem = URLQueryItem(
                        name: $0.key,
                        value: "\($0.value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                    )
                    urlComponents.queryItems?.append(queryItem)
                }
            }
            request.url = urlComponents.url
        }

        if request.value(forHTTPHeaderField: "Content-Type") == nil {
            request.setValue(
                "application/json; charset=utf-8",
                forHTTPHeaderField: "Content-Type"
            )
        }
    }
}

enum ParametersEncoding {
    case urlEncoding
    case jsonEncoding
    case bouthTypeEncoding

    func encode(request: inout URLRequest, bodyParams: Parameters?, urlParams: Parameters?) throws {
        do {
            switch self {
            case .urlEncoding:
                guard let params = urlParams else { return }
                try URLParametersEncoder().encode(request: &request, params: params)
            case .jsonEncoding:
                guard let params = bodyParams else { return }
                try JSONParametersEncoder().encode(request: &request, params: params)
            case .bouthTypeEncoding:
                guard let body = bodyParams, let urlParams = urlParams else { return }
                try URLParametersEncoder().encode(request: &request, params: urlParams)
                try JSONParametersEncoder().encode(request: &request, params: body)
            }
        } catch {
            throw error
        }
    }
}
