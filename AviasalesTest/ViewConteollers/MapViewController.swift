import UIKit
import Stevia
import MapKit

typealias GeoData = (CLLocationCoordinate2D, String)

class MapViewController: UIViewController {
    private lazy var mapView = MKMapView(frame: .zero)

    var planeAnnotation: MKPointAnnotation = MKPointAnnotation()
    var planeAnnotationPosition = 0
    var flightpathPolyline: MKGeodesicPolyline = MKGeodesicPolyline()
    var plainDirection: CLLocationDirection = 0
    var plainAnnotationView = MKAnnotationView()
    
    private var displayLink: CADisplayLink?

    private let location: Location
    private let iata: String
    init(location: Location, iata: String) {
        self.location = location
        self.iata = iata
        super.init(nibName: nil, bundle: nil)
        setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) { fatalError() }

    private var routes: [GeoData] {
        return [
            (CLLocation(latitude: 59.806084, longitude: 30.3083).coordinate, "LED"),
            (CLLocation(latitude: location.lat, longitude: location.lon).coordinate, iata)
        ]
    }

    private func setupSubviews() {
        view.sv(mapView)
        mapView.fillContainer()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureMap()
        updatePlanePosition()
        displayLink = CADisplayLink(target: self, selector: #selector(updatePlanePosition))
        displayLink?.add(to: .current, forMode: .common)
    }

    private func configureMap() {
        mapView.delegate = self
        mapView.showsCompass = false
        mapView.isRotateEnabled = false
        mapView.region = regionFor(coordinates: routes.compactMap { $0.0 })
        flightpathPolyline = MKGeodesicPolyline(
            coordinates: routes.compactMap { $0.0 },
            count: routes.count
        )
        mapView.addOverlay(flightpathPolyline)
        planeAnnotation = MKPointAnnotation()
        mapView.addAnnotation(planeAnnotation)
        addPointAnnotations(routes: routes)
    }
}

extension MapViewController {
    @objc private func updatePlanePosition() {
        let step = 5
        guard (planeAnnotationPosition + step < flightpathPolyline.pointCount) else {
            displayLink?.isPaused = true
            displayLink?.invalidate()
            return
        }
        let points = flightpathPolyline.points()
        let previousPoints = points[planeAnnotationPosition]
        planeAnnotationPosition += step
        let nextPoints = points[planeAnnotationPosition]
        plainDirection = directionBetweenPoints(sourcePoint: previousPoints, nextPoints)
        planeAnnotation.coordinate = nextPoints.coordinate
        UIView.animate(withDuration: 0.02) {
            self.plainAnnotationView.transform = CGAffineTransform(
                rotationAngle: CGFloat(self.degreesToRadians(degrees: self.plainDirection))
            )
        }
    }

    private func directionBetweenPoints(sourcePoint: MKMapPoint, _ destinationPoint: MKMapPoint) -> CLLocationDirection {
        let x = destinationPoint.x - sourcePoint.x
        let y = destinationPoint.y - sourcePoint.y
        return radiansToDegrees(radians: atan2(y, x)).truncatingRemainder(dividingBy: 360) + 360
    }

    private func radiansToDegrees(radians: Double) -> Double {
        return radians * 180 / .pi
    }

    private func degreesToRadians(degrees: Double) -> Double {
        return degrees * .pi / 180
    }

    private func regionFor(coordinates coords: [CLLocationCoordinate2D]) -> MKCoordinateRegion {
        var r = MKMapRect.null
        for i in 0 ..< coords.count {
            let p = MKMapPoint(coords[i])
            r = r.union(MKMapRect(x: p.x, y: p.y, width: 0, height: 0))
        }
        var region = MKCoordinateRegion(r)
        region.span = MKCoordinateSpan(
            latitudeDelta: region.span.latitudeDelta * 1.5,
            longitudeDelta: region.span.longitudeDelta * 1.5
        )
        return region
    }

    private func addPointAnnotations(routes: [GeoData]) {
        routes.forEach {
            let annotation = MKPointAnnotation()
            annotation.title = $0.1
            annotation.coordinate = $0.0
            mapView.addAnnotation(annotation)
        }
    }

    private func viewFor(annotation: MKAnnotation) -> MKAnnotationView? {
        guard
            annotation.coordinate == routes[0].0 || annotation.coordinate == routes[1].0
        else {
            let id = "Plane"
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: id)
                ?? MKAnnotationView(annotation: annotation, reuseIdentifier: id)
            annotationView.annotation = annotation
            annotationView.image = R.image.plane()!
            plainAnnotationView = annotationView
            return plainAnnotationView
        }
        let label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 60, height: 30)))
        label.font = Font.name.font
        label.textColor = R.color.blueDark()
        label.text = annotation.title!
        label.textAlignment = .center
        label.backgroundColor = R.color.white()
        label.layer.borderColor = R.color.blueDark()?.cgColor
        label.layer.cornerRadius = 12
        label.clipsToBounds = true
        label.layer.borderWidth = 2
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.title!)
        annotationView.annotation = annotation
        annotationView.image = UIImage.imageWithLabel(label)
        return annotationView
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else { return MKOverlayRenderer() }
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = R.color.blueDark()
        renderer.lineDashPhase = 2.0
        renderer.lineWidth = 6.0
        renderer.alpha = 0.8
        renderer.lineDashPattern = [0, 8]
        return renderer
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return viewFor(annotation: annotation)
    }
}

extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
