import UIKit
import Stevia

protocol SearchAirportsViewControllerDelegate: AnyObject {
    func didSelectItem(_ location: Location, iata: String)
    func dismissSearch()
}

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}

class SearchAirportsViewController: UIViewController {
    private let searchedPoint: String
    private lazy var airaportsView = AirportsTableView(topInset: 20, delegate: self)

    private let interactor = Interactor()

    private var panGesture: UIPanGestureRecognizer {
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handleGesture))
        recognizer.maximumNumberOfTouches = 1
        recognizer.maximumNumberOfTouches = 1
        return recognizer
    }

    private weak var delegate: SearchAirportsViewControllerDelegate?
    private let networkingService: AviasalesNetworkService
    init(
        searchedPoint: String,
        networkingService: AviasalesNetworkService,
        delegate: SearchAirportsViewControllerDelegate?
    ) {
        self.delegate = delegate
        self.searchedPoint = searchedPoint
        self.networkingService = networkingService
        super.init(nibName: nil, bundle: nil)
        transitioningDelegate = self
    }

    required init?(coder aDecoder: NSCoder) { fatalError() }

    private func setupSubviews() {
        view.backgroundColor = .clear
        airaportsView.table.layer.maskedCorners = [
            .layerMinXMinYCorner,
            .layerMaxXMinYCorner
        ]
        airaportsView.table.layer.cornerRadius = 20
        view.sv(airaportsView)
        view.layout(
            150,
            |airaportsView|,
            0
        )
        view.addGestureRecognizer(panGesture)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        networkingService.search(searchedPoint) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .failure(error):
                self.airaportsView.errorDescription = error.description
            case let .success(data):
                self.airaportsView.data = data
            }
        }
    }
}

extension SearchAirportsViewController {
    @objc private func handleGesture(pan: UIPanGestureRecognizer) {
        let percentThreshold: CGFloat = 0.3

        let translation = pan.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)

        switch pan.state {
        case .began:
            interactor.hasStarted = true
            delegate?.dismissSearch()
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish ? interactor.finish() : interactor.cancel()
        default:
            break
        }
    }
}

extension SearchAirportsViewController: AirportsTableViewDelegate {
    func didSelectItem(_ location: Location, iata: String) {
        delegate?.didSelectItem(location, iata: iata)
    }
}

extension SearchAirportsViewController: UIViewControllerTransitioningDelegate {
    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        return SearchViewMoveInAnimator()
    }
    
    func animationController(
        forDismissed dismissed: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        return SearchViewMoveOutAnimator()
    }
    
    func interactionControllerForDismissal(
        using animator: UIViewControllerAnimatedTransitioning
    ) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}
