import UIKit
import Stevia

protocol MainViewControllerDelegate: AnyObject {
    func searchAirports(in city: String)
}

class MainViewController: UIViewController {
    var padding: UIEdgeInsets {
        return .init(top: 0, left: 20, bottom: 0, right: 20)
    }

    private lazy var infoLabel = UILabel().style(infoLabelStyle)
    private lazy var searchButton = UIButton().style(searchButtonStyle)
    private lazy var textField = PaddingTextField(padding: padding).style(textFieldStyle)

    private func textFieldStyle(v: PaddingTextField) {
        v.textColor = R.color.black()
        v.tintColor = R.color.black()
        v.layer.cornerRadius = 12
        v.backgroundColor = R.color.white()
        v.borderStyle = .none
        v.clearButtonMode = .whileEditing
        v.clipsToBounds = true
        v.autocorrectionType = .yes
        v.font = Font.name.font
        v.leftViewMode = .always
        v.height(40)
    }
    private func searchButtonStyle(v: UIButton) {
        let attributedText = NSAttributedString(
            string: "Проложить маршрут",
            attributes: [
                NSAttributedString.Key.font: Font.iata.font,
                NSAttributedString.Key.foregroundColor: R.color.white()!
            ]
        )
        v.setAttributedTitle(attributedText, for: [])
        v.backgroundColor = R.color.orange()
        v.layer.cornerRadius = 10
        v.height(50)
    }
    private func infoLabelStyle(l: UILabel) {
        l.textAlignment = .center
        l.textColor = R.color.white()
        l.font = Font.location.font
        l.numberOfLines = 0
        l.text = """
        Все очень просто :)
        Вбейте в поле город или страну, где хотите побывать, выберите аэропорт из списка и проложите маршрут!
        Отправная точка:
        `Солнечный Санкт - Петербург`.
        """
    }

    private weak var delegate: MainViewControllerDelegate?
    init(delegate: MainViewControllerDelegate?) {
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
        setupSubview()
        hideKeyboardWhenTappedAround()
    }

    required init?(coder aDecoder: NSCoder) { fatalError() }

    override func viewDidLoad() {
        super.viewDidLoad()
        searchButton.addTarget(self, action: #selector(tapOnSearch), for: .touchUpInside)
    }

    private func setupSubview() {
        view.backgroundColor = R.color.blueDark()
        view.sv(searchButton, textField, infoLabel)
        searchButton.centerVertically()
        view.layout(
            |-25-textField-25-|
        )
        textField.Bottom == searchButton.Top - 20
        searchButton.Left == textField.Left
        searchButton.Right == textField.Right
        infoLabel.Left == textField.Left
        infoLabel.Right == textField.Right
        infoLabel.Bottom == textField.Top - 20
        infoLabel.setContentHuggingPriority(.required, for: .vertical)
    }

    @objc private func tapOnSearch() {
        guard let text = textField.text, text != "" else {
            print("There is no text from user!")
            return
        }
        textField.resignFirstResponder()
        delegate?.searchAirports(in: text)
    }
}
