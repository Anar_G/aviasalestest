import Foundation

typealias Ecodable = Equatable & Codable

struct Airport: Ecodable {
    let name: String?
    let airportName: String?
    let iata: String
    let location: Location
}

struct Location: Ecodable {
    let lat: Double
    let lon: Double
}
